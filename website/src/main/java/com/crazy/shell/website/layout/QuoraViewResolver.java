package com.crazy.shell.website.layout;

import org.apache.commons.lang.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.context.Context;
import org.springframework.web.servlet.view.velocity.VelocityView;

import javax.servlet.http.HttpServletResponse;
import java.io.StringWriter;

/**
 * Created by xForMe on 2016/1/21.
 */
public class QuoraViewResolver extends VelocityView {
    private String screenContentKey = "screenContent";

    public String getScreenContentKey() {
        return screenContentKey;
    }

    @Override
    protected void mergeTemplate(Template template, Context context, HttpServletResponse response) throws Exception {
        StringWriter writerBuffer = new StringWriter(1024);
        template.merge(context,writerBuffer);

        String layout = (String) context.get("layout");

        if(StringUtils.isNotEmpty(layout)){
            context.put("templateName",template.getName());
            context.put(getScreenContentKey(),writerBuffer.toString());
            Template _t = getTemplate(layout);
            _t.merge(context,response.getWriter());
            return;
        }
    }
}
