package com.crazy.shell.website.controller.expect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.handler.Handler;

/**
 * 项目名称：shell-crm
 * 创建人: crazy.huang
 * 创建时间：2016/1/20.
 */

public class ControllerExceptionResolver implements HandlerExceptionResolver {
    private final Logger logger = LoggerFactory.getLogger(ControllerExceptionResolver.class);

    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object o, Exception e) {

//        ex.printStackTrace();
//
//        response.setStatus(404);
//
//        ModelAndView mv = new ModelAndView();
//
//        String message = "呃...你想访问的页面出了点问题～";
//
//        if (ex.getClass().equals(java.lang.IllegalArgumentException.class)) {
//            message = ex.getMessage();
//        }
//        logger.error("系统异常 >> " + ex.getMessage() + "  url=" + request.getRequestURI());
//
//        mv.addObject("message", message);
//        mv.setViewName("error/error");
//
//        return mv;
        e.printStackTrace();
        response.setStatus(404);
        ModelAndView mv = new ModelAndView();

        String msg = "呃...你想访问的页面出了点问题～";
        if(e.getClass().equals(java.lang.IllegalArgumentException.class)){
            msg = e.getMessage();
        }
        logger.error("系统异常 >>" + e.getMessage() +" url="+request.getRequestURI());

        mv.addObject("message",msg);
        mv.setViewName("error/error");
        return mv;
    }
}
