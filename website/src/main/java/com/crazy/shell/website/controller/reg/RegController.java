package com.crazy.shell.website.controller.reg;

import com.alibaba.dubbo.config.annotation.Reference;
import com.baomidou.kisso.annotation.Action;
import com.baomidou.kisso.annotation.Login;
import com.crazy.shell.api.guest.GuestProvider;
import com.crazy.shell.api.users.UsersProvider;
import com.crazy.shell.common.controller.BaseController;
import com.crazy.shell.common.result.ActionResult;
import com.crazy.shell.dao.bean.UserEnum;
import com.crazy.shell.dao.entity.Company;
import com.crazy.shell.dao.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/5.
 */
@Controller
@RequestMapping("/reg")
public class RegController extends BaseController {

    @Autowired
    private UsersProvider usersProvider;

    @Value("${sso.login.url}")
    private String loginUrl;



    @Login(action = Action.Skip)
    @RequestMapping("/index.htm")
    public String toReg(ModelMap model){
        model.put("layout","/layout/empty.vm");
        model.put("loginUrl",loginUrl);
        return "/reg/index";
    }

    @Login(action = Action.Skip)
    @RequestMapping("/reg.htm")
    public String register(ModelMap model, Company company, Users users){
        users.setRoleId(UserEnum.RoleEnum.Register.getKey());
        ActionResult result = usersProvider.register(company,users);
        if(!result.getSuccess()){
            model.put("msg",result.getMessages());
            return "/result/500";
        }
        return redirectTo("/index.htm");
    }



}
