package com.crazy.shell.api.comService;

import com.crazy.shell.api.base.BaseProvider;
import com.crazy.shell.common.result.ActionResult;
import com.crazy.shell.dao.entity.ComService;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/7.
 */
public interface ComServiceProvider extends BaseProvider<ComService> {

    /**
     * 根据对象查询
     * @param comService
     * @return
     */
    public ActionResult findByObj(ComService comService);
}
