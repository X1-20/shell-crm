package com.crazy.shell.server.comService;

import com.alibaba.dubbo.config.annotation.Service;
import com.crazy.shell.api.comService.ComServiceProvider;
import com.crazy.shell.common.persistent.service.BaseService;
import com.crazy.shell.common.persistent.service.impl.BaseServiceImpl;
import com.crazy.shell.common.result.ActionResult;
import com.crazy.shell.dao.bean.BaseEnum;
import com.crazy.shell.dao.entity.ComService;
import com.crazy.shell.server.base.BaseProviderImpl;
import com.crazy.shell.service.comService.ComSerService;
import com.crazy.shell.service.company.CompanyService;

import javax.annotation.Resource;
import java.util.List;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/7.
 */
@Service
public class ComServiceProviderImpl extends BaseProviderImpl<ComService> implements ComServiceProvider {
    @Resource
    private ComSerService comSerService;

    @Override
    public BaseService<ComService> getBaseService() {
        return comSerService;
    }


    @Override
    public ActionResult findByObj(ComService comService) {
        ActionResult result = new ActionResult();
        comService.setEnable(BaseEnum.EnableEnum.Enable.getKey());

        List<ComService> comServiceList = comSerService.findByObj(comService);
        if(comServiceList.size() > 0){
            result.setSuccess(true);
            result.attach("data",comServiceList);
            return result;
        }

        result.setMessages("not find comservice by record");
        return result;
    }
}
