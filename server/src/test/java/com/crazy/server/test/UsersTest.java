package com.crazy.server.test;

import com.crazy.server.AbstractUnitTest;
import com.crazy.shell.common.result.ActionResult;
import com.crazy.shell.dao.entity.Company;
import com.crazy.shell.dao.entity.Users;
import com.crazy.shell.service.company.CompanyService;
import com.crazy.shell.service.users.UsersService;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

/**
 * 项目名称：shell-crm
 * 创建人: crazy.huang
 * 创建时间：2016/3/3.
 */

public class UsersTest extends AbstractUnitTest {
    private  static Logger logger = LoggerFactory.getLogger(UsersTest.class);
    @Resource
    private UsersService usersService;

    @Test
    public void register(){
        Users users = new Users();
        users.setUsername("18317170481");
        users.setPassword("password");
        Company company = new Company();
        company.setName("小贝壳科技有限公司");
        ActionResult result = usersService.addTran(company,users);
        logger.info(" result {}" ,result);

    }
}
