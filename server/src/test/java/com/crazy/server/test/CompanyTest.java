package com.crazy.server.test;

import com.crazy.server.AbstractUnitTest;
import com.crazy.shell.dao.entity.Company;
import com.crazy.shell.service.company.CompanyService;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

/**
 * 项目名称：shell-crm
 * 创建人: crazy.huang
 * 创建时间：2016/3/3.
 */

public class CompanyTest extends AbstractUnitTest {
    private  static Logger logger = LoggerFactory.getLogger(CompanyTest.class);
    @Resource
    private CompanyService companyService;

    @Test
    public void selectById(){
        logger.debug("guest");
        Company company = companyService.getByKey(1);
        logger.info(" company {}" ,company);

    }
}
