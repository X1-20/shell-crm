package com.crazy.shell.dao.bean;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建日期 : 16/8/8
 * 更新人:
 * 更新时间:
 * 描述:
 */
public class CacheConstants {
    /**
     * 人员缓存key
     */
    public static final String CACHE_USERS = "CRM_CACHE_USERS_{}";

    /**
     * 企业缓存KEY
     */
    public static final String CACHE_COMPANY = "CRM_CACHE_COMPANY_{}";
}
