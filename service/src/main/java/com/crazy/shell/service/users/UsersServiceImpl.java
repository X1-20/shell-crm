package com.crazy.shell.service.users;

import com.alibaba.fastjson.JSON;
import com.crazy.shell.common.exception.ServiceException;
import com.crazy.shell.common.persistent.dao.criteria.Criteria;
import com.crazy.shell.common.persistent.dao.entity.Example;
import com.crazy.shell.common.persistent.dao.mapper.Mapper;
import com.crazy.shell.common.persistent.service.BaseService;
import com.crazy.shell.common.persistent.service.impl.BaseServiceImpl;
import com.crazy.shell.common.redis.RedisClientTemplate;
import com.crazy.shell.common.result.ActionResult;
import com.crazy.shell.dao.bean.BaseEnum;
import com.crazy.shell.dao.bean.CacheConstants;
import com.crazy.shell.dao.bean.users.UserDetail;
import com.crazy.shell.dao.entity.Company;
import com.crazy.shell.dao.entity.Users;
import com.crazy.shell.dao.mapper.mybatis.CompanyMapper;
import com.crazy.shell.dao.mapper.mybatis.UsersMapper;
import com.crazy.shell.service.company.CompanyService;
import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.MessageFormat;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/7.
 */
@Component
public class UsersServiceImpl extends BaseServiceImpl<Users> implements UsersService {

    private static Logger logger = LoggerFactory.getLogger(UsersServiceImpl.class);

    @Resource
    private UsersMapper usersMapper;

    @Resource
    private CompanyService companyService;

    @Resource
    private RedisClientTemplate redisTemp;

    @Override
    public Mapper<Users> getMapper() {
        return usersMapper;
    }

    @Override
    public int countOrObj(Users users) {
        //todo example 实现or
        int count = usersMapper.selectCountByExample(getExample(users));
        return count;
    }

    @Override
    public Users getByObj(Users record) {

        return super.getByObj(record);
    }

    @Override
    public ActionResult addTran(Company company, Users users) {
        ActionResult result = new ActionResult();
        if(StringUtils.isEmpty(users.getUsername())){
            logger.error("user name  is null ,user {}",users);
            result.setMessages("用户名不为空");
            return result;
        }

        if(StringUtils.isEmpty(users.getPassword())){
            logger.error("user password  is null ,user {}",users);
            result.setMessages("密码不为空");
            return result;
        }

        int row = insert(users);
        if(row == 0){
            logger.error("insert failure ,user {}",users);
            result.setMessages("插入失败");
            return result;
        }
        company.setUserId(users.getId());
        row  = companyService.insert(company);
        if(row == 0){
            logger.error("insert failure ,company {}",company);
            result.setMessages("插入失败");
            return result;
        }
        result.setSuccess(true);
        result.setMessages("注册成功");
        return result;
    }

    @Override
    public UserDetail getLoginUserInfo (Integer uid) {
        
        // TODO: 16/8/8
//        Users cfg = new Users();
//        cfg.setId(uid);
//        cfg.setEnable(BaseEnum.EnableEnum.Enable.getKey());
//        Users tar = this.getByObj(cfg);
//        if(tar == null){
//            throw new ServiceException("not find users by uid : "+ uid);
//        }
//        Company c_cfg = new Company();
//        c_cfg.setUserId(uid);
//        c_cfg.setEnable(BaseEnum.EnableEnum.Enable.getKey());
//        Company com = companyService.getByKey(c_cfg);
//        if(com == null){
//            throw new ServiceException("not find company by uid : "+ uid);
//        }


        return null;
    }

    @Override
    public Users getFromCache (Integer uid) {
        String key = MessageFormat.format(CacheConstants.CACHE_USERS,uid);
        if(redisTemp.exists(key)){
            String json = redisTemp.get(key);
            return JSON.parseObject(json,Users.class);
        }

        Users cfg = new Users();
        cfg.setId(uid);
        cfg.setEnable(BaseEnum.EnableEnum.Enable.getKey());
        Users tar = this.getByObj(cfg);
        if(tar == null){
            throw new ServiceException("not find users by uid : "+ uid);
        }
        String reVal = redisTemp.set(key,JSON.toJSONString(tar));
        return tar;
    }


    private Example getExample(Users users){
        Example example = new Example(Users.class);
        Criteria criteria = example.createCriteria();
        if(StringUtils.isNotEmpty(users.getUsername()))
            criteria.andEqualTo("username",users.getUsername());

        return example;

    }
}
