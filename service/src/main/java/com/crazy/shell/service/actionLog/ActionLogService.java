package com.crazy.shell.service.actionLog;

import com.crazy.shell.dao.entity.repository.ActionLog;

/**
 * shell-crm
 * 创建人 : creazier.huang
 * 创建时间 : 16/7/21.
 */
public interface ActionLogService {

    public ActionLog save(ActionLog log);
}
